package test;

import org.junit.Test;
import roman.RomanConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.*;


public class FunctionalityTest {
	private RomanConverter roman;
    @Test
    public void testToRomanRandom(){
    	roman = new RomanConverter();
        Random rand = new Random();
        int x = rand.nextInt(3998) + 1;
        assertNotNull(roman.toRoman(x));
    }
    
    @Test (expected=IllegalArgumentException.class)
    public void testToRomanRangeBoundary(){
    	roman = new RomanConverter();
        roman.toRoman(4000);
        roman.toRoman(-1);
        fail();
    }
    
    @Test
    public void testFromRomanValidInput(){
    	roman = new RomanConverter();
        assertEquals(roman.toRoman(1),"I");

    }
    
    @Test (expected=IllegalArgumentException.class)
    public void testFromRomanInvalidInput(){
    	roman = new RomanConverter();
        roman.fromRoman("A");
        fail();
    }
    
    @Test
    public void testAllToRoman(){
    	roman = new RomanConverter();
        int to;
        String str;
        for (int i=1; i < 4000; ++i) {
            str = roman.toRoman(i);
            to = roman.fromRoman(str);
            assertEquals(i, to);
        }
    }
    
    
    @Test
    public void testToRomanUpper(){
        roman = new RomanConverter();
        Random rand = new Random();
        int num = rand.nextInt(3998) + 1;
        char[] strArr = roman.toRoman(num).toCharArray();
        for (int i = 0; i < strArr.length; i++){
            assertTrue(Character.isUpperCase(strArr[i]));
        }
    }
    
    @Test (expected=IllegalArgumentException.class)
    public void testFromRomanUpper(){
    	roman = new RomanConverter();
        roman.fromRoman("x");
        fail();
    }

}
