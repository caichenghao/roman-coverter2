package test;



import static org.junit.Assert.*;

import org.junit.*;
import org.junit.internal.runners.statements.Fail;

import roman.*;


public class InterfaceTest {

	
	private RomanConverter roman;
	
	//negative -100
	@Test(expected = IllegalArgumentException.class)
	public void testSmallOutOfRange() {
		roman = new RomanConverter();
		roman.toRoman(-100);
	}
	//32
	@Test
	public void testToRomanMid() {
		
		roman = new RomanConverter();
		assertEquals("XXXII", roman.toRoman(32));
	}
	//0
    @Test (expected = IllegalArgumentException.class)
    public void testToRomanZero(){
    	roman = new RomanConverter();
    	roman.toRoman(0);
    }  
    
    //9900
	@Test(expected = IllegalArgumentException.class)
	public void testLargeOutOfRange() {
		roman = new RomanConverter();
		roman.toRoman(9900);
	}
	
	
    //CXXIII long string
	@Test
	public void testFromRomanLong() {
		roman = new RomanConverter();
		assertEquals(3576, roman.fromRoman("MMMDLXXVI"));
	}
    //CXXIII short string
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalRoman() {
		roman = new RomanConverter();
		roman.fromRoman("a");
	}
	//null
	@Test
    public void testFromRomanNull(){
    	roman = new RomanConverter();
    	try{
        roman.fromRoman(null);
        fail("expected IllegalArgumentException");
    	}catch(NullPointerException e){
    		
    	}
    	
    	
    }
    //empty
	@Test
    public void testFromRomanEmpty(){
		roman = new RomanConverter();
		assertEquals(0, roman.fromRoman(""));
    	
    }

}
